<?php

/**
 * @file
 * Implementation of 'Long Text' property behavior plugin for eck.
 */

$plugin = array(
  'label' => 'Blob',
  'default_widget' => 'eck_properties_extras_blob_property_widget',
  'default_formatter' => 'eck_properties_extras_blob_property_formatter',
  'property_form' => 'eck_properties_extras_blob_property_form',
);

/**
 * Implements 'default_widget' plugin.
 */
function eck_properties_extras_blob_property_widget($property, $vars) {
  if ($settings = eck_properties_extras_property_settings($vars['entity']->entityType(), $property)) {
    return array(
      '#type' => 'textarea',
      '#cols' => $settings['cols'],
      '#rows' => $settings['rows'],
      '#required' => $settings['required'],
      '#title' => t('@label', array('@label' => $vars['properties'][$property]['label'])),
      '#default_value' => eck_properties_extras_get_property_value($vars['entity'], $property),
      '#access' => eck_properties_extras_property_access($vars['entity'], $property, 'edit'),
    );
  }
}

/**
 * Implements 'default_formatter' plugin.
 */
function eck_properties_extras_blob_property_formatter($property, $vars) {
  $value = eck_properties_extras_get_property_value($vars['entity'], $property);
  $format_id = NULL;
  if ($settings = eck_properties_extras_property_settings($vars['entity']->entityType(), $property)) {
    if (isset($settings['text_format'])) {
      $format_id = $settings['text_format'];
    }
  }
  $safe_value = check_markup($value, $format_id);

  return eck_properties_extras_theme_property($safe_value, $property, $vars['entity']);
}

/**
 * Implements 'property_form' plugin.
 */
function eck_properties_extras_blob_property_form($vars) {
  $settings = isset($vars['settings']) ? $vars['settings'] : NULL;
  // Property specific form.
  $form = array();
  $form['required'] = array(
    '#title' => t('Required property'),
    '#type' => 'checkbox',
    '#default_value' => isset($settings['required']) ? $settings['required'] : NULL,
  );
  $form['cols'] = array(
    '#title' => t('Columns'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#element_validate' => array('element_validate_integer_positive'),
    '#description' => t('How many columns wide the textarea should be.'),
    '#default_value' => isset($settings['cols']) ? $settings['cols'] : NULL,
  );
  $form['rows'] = array(
    '#title' => t('Rows'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#element_validate' => array('element_validate_integer_positive'),
    '#description' => t('How many rows high the textarea should be.'),
    '#default_value' => isset($settings['rows']) ? $settings['rows'] : NULL,
  );
  $format_options = array();
  foreach (filter_formats() as $name => $format) {
    $format_options[$name] = $format->name;
  }
  $form['text_format'] = array(
    '#title' => t('Text format'),
    '#type' => 'select',
    '#required' => TRUE,
    '#description' => t('The text format you would like to present to the user when creating this property. Plain text is the fallback format in case the user has no access to this text forma.'),
    '#options' => $format_options,
    '#default_value' => isset($settings['text_format']) ? $settings['text_format'] : NULL,
  );

  return $form;
}
