<?php
/**
 * @file
 * Hidden Property behavior plugin definition.
 */

$plugin = array(
  'label' => 'Hidden',
  'entity_info' => 'eck_properties_extras_hidden_property_entity_info',
  'entity_save' => 'eck_properties_extras_hidden_property_entity_insert',
  'property_info' => 'eck_properties_extras_hidden_property_property_info',
);

/**
 * Entity info.
 */
function eck_properties_extras_hidden_property_entity_info($property, $var) {
  $info = $var;
  $info['entity keys']['hidden'] = $property;
  return $info;
}

/**
 * Entity insert.
 */
function eck_properties_extras_hidden_property_entity_insert($property, $vars) {
  $entity = $vars['entity'];
  if (isset($vars['form_state'])) {
    //empty($entity->{$property}) && 
    //dd(array_keys($vars['form_state']), 'VARS');
  }
}

/**
 * Property info.
 */
function eck_properties_extras_hidden_property_property_info($property, $vars) {
  $vars['properties'][$property]['label'] = t('Hidden');
  $vars['properties'][$property]['type'] = 'text';
  $vars['properties'][$property]['description'] = t('A property to be attached to the entity but should remain hidden from the user.');
  return $vars;
}

