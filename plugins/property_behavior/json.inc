<?php
/**
 * @file
 * json_data Property behavior plugin definition.
 */

$plugin = array(
  'label' => 'JSON data',
  'entity_info' => 'eck_properties_extras_json_data_property_entity_info',
  'entity_save' => 'eck_properties_extras_json_data_property_entity_insert',
  'property_info' => 'eck_properties_extras_json_data_property_property_info',
);

/**
 * Entity info.
 */
function eck_properties_extras_json_data_property_entity_info($property, $var) {
  $info = $var;
  $info['entity keys']['json_data'] = $property;
  return $info;
}

/**
 * Entity insert.
 */
function eck_properties_extras_json_data_property_entity_insert($property, $vars) {
  $entity = $vars['entity'];
  if (isset($vars['form_state'])) {
    //empty($entity->{$property}) && 
    // dd(array_keys($vars['form_state']), 'VARS');
    // $vars['form_state']['input'] <-- contains the input from the form
    // $vars['form_state']['values']['entity'] <-- contains the full entity object
    // dd($vars['form_state']['values'], 'VALUES');
    // $test = array('ValueA' => 'Something B');
    // $entity->{$property} = json_encode($test);
  }
}

/**
 * Property info.
 */
function eck_properties_extras_json_data_property_property_info($property, $vars) {
  $vars['properties'][$property]['label'] = t('JSON data');
  $vars['properties'][$property]['type'] = 'text';
  $vars['properties'][$property]['description'] = t('A json_data property object in JSON format.');
  return $vars;
}

